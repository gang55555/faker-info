## 骗子张怡旸

最近报考摩托车驾照，被一个叫张怡旸的骗子给骗了，在这里给大家曝光一下他的相关信息，大家以后小心此人。

和骗子微信聊天，说他在重庆合川区开车行的：铭泰车行；后来知道被骗后我在搞得地图找到这个铭泰车行，并根据地图上门面照片上的电话打电话确认，那个老板说那确实是铭泰车行，他不认识张怡旸。

那个骗子的朋友圈发的照片上有些店铺名，我搜了下，就是重庆合川区，但是和铭泰车行隔一条街。也可能照片是从网上偷的

<img src="./difang.jpg" style="zoom:25%;" />

根据骗子之前抖音发的相关信息，骗子可能在陕西国际商贸学院，他的微信公众号注册也和这个地方相关。

<img src="./dy1.jpg" style="zoom:25%;" />



### 骗子姓名：张怡旸(通过支付宝转账显示的姓名)

<img src="./sfz.jpg" style="zoom:25%;" />

### 电话：15619386468（陕西西安联通）

### QQ：京华倦客 1198378554

![](./qqxinxi.png)

### 微信号：hcmtch1  新的微信号：a_210515

<img src="./wx.jpg" style="zoom:25%;" />

### 抖音号：z1198378554

<img src="./dy.jpg" style="zoom:25%;" />

### 支付宝账号：1198378554@qq.com(昵称 小金龙，合川车行，铭泰车行，合川铭泰车行等)

<img src="zhifubaoma.jpg" style="zoom:25%;" />

<img src="./zhifubaomingcheng.png" style="zoom:25%;" />

### 他开的公众号信息：（目前已注销）

<img src="./wxgzh.jpg" style="zoom:25%;" />

<img src="./wxgzhinfo.jpg" style="zoom:25%;" />

### 转账记录：

<img src="./jiaoyijilu.jpg" style="zoom:25%;" />

### 支付宝投诉诈骗：

<img src="./zfbts.jpg" style="zoom:25%;" />

### 他的朋友圈：

<img src="./wxpyq1.jpg" style="zoom:50%;" />

<img src="./wxpyq2.jpg" style="zoom:50%;" />

<img src="./wxpyq3.jpg" style="zoom:50%;" />